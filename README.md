E012 &ndash; Plexon-like sorting
================================

[![](https://img.shields.io/badge/Jira-VIZPIKE--26-blue)](https://pedroasad.atlassian.net/browse/VIZPIKE-26)

## Requirements

* Python 3.8 (with Jupyter notebook)
* [Poetry](https://python-poetry.org/)
* [DVC](https://dvc.org)
* [Rclone](https://rclone.org) (optional)

## Instructions

1. Create a virtualenv, install dependencies, and register the kernel with Jupyter notebook with

   ```bash
   poetry install
   poetry run -- python -m ipykernel install --user --name "E012-Plexon-like_sorting"
   jupyter notebook
   ```
